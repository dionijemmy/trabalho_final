<?php
session_start();

require 'vendor/autoload.php';
use DebugBar\StandardDebugBar;

include "include/db.php";
include "include/navigation.php";
include "include/header.php";
 ?>
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

            <?php
            $per_page = 2;

            if(isset($_GET['page'])){
                $page = $_GET['page'];
            } else {
                $page = "";
            }

            if($page == ""  || $page == 1){
                $page_1 = 0;
            } else {
                $page_1 = ($page * $per_page) - $per_page;
            }

$post_query_count = "SELECT * FROM post WHERE post_status = 'published'";
$find_count = mysqli_query($connection, $post_query_count);
$count = mysqli_num_rows($find_count);

if($count < 1) {
    echo "<h1 class='text-center'>the posts are coming</h1>";
} else {

$count = ceil($count / $per_page);



?>
            <?php 
            $query = "SELECT * FROM post WHERE post_status = 'published' LIMIT $page_1, $per_page";
            $select_all_post_query = mysqli_query($connection,$query);
            
            while($row = mysqli_fetch_assoc($select_all_post_query)){
                $post_id = $row['post_id'];
                $post_title = $row['post_title'];
                $post_author = $row['post_author'];
                $post_date = $row['post_date'];
                $post_image = $row['post_image'];
                $post_content = substr($row['post_content'],0,100);
            
                ?>

                <!-- First Blog Post -->
                <h2 class="titulo">
                    <a href="post.php?p_id=<?php echo $post_id ?>"><?php echo $post_title;?></a>
                </h2>
                <p class="lead">
                    por <a href="author_post.php?author=<?php echo $post_author;?>&p_id=<?php echo $post_id;?>"><?php echo $post_author;?></a>
                </p>
                <p><span class="glyphicon glyphicon-time"></span> <?php echo "Posted on ", $post_date?></p>
                <hr>

                <a href="post.php?p_id=<?php echo $post_id ?>"></a>
                <img class="img-responsive" src="images/<?php echo $post_image; ?>" alt="">
                <hr>
                <p><?php echo $post_content ?></p>
                <a class="btn btn-primary" href="post.php?p_id=<?php echo $post_id ?>">Ler mais<span class="glyphicon glyphicon-chevron-right"></span></a>
                <hr> 
           <?php } }?>
        


            </div>

            <!-- Blog Sidebar Widgets Column -->
     <?php include "include/sidebar.php"; ?>

        </div>
        <!-- /.row -->

        <hr>

        <ul class="pager">

        <?php
        for($i =1; $i <= $count; $i++){
            echo "<li><a class='pagina' href='index.php?page={$i}'>{$i}</a></li>";


        }
        ?>
            <!-- <li><a href="">1</a></li>        
            <li><a href="">2</a></li>   -->
        
        
        </ul>

        <!-- Footer -->
       <?php
include "include/footer.php"; ?>