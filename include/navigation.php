<?php
include_once "admin/functions.php"; 
?>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php" style="color: rgba(0, 255, 170, 0.7); font-size: 24px;">TECHNOBLOG</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php

                $query = "SELECT * FROM categories LIMIT 3";
                $select_all_cat_query = mysqli_query($connection, $query);

                while ($row = mysqli_fetch_assoc($select_all_cat_query)) {
                    $cat_title = $row['cat_title'];

                    echo "<li><a href='#'>{$cat_title}</a></li>";
                }
                ?>
                <?php if(logged()): ?>
                    <li>
                        <a href="admin">admin</a>
                    </li>

                    <li>
                        <a href="include/logout.php">sair</a>
                    </li>

                <?php else: ?>

                    <li>
                        <a href="login.php">Login</a>
                    </li>
                <?php endif; ?>
                <li>
                    <a href="registration.php">Cadastrar</a>
                </li>

                <li>
                    <a href="contact.php">Contato</a>
                </li>
                <!-- <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li> -->
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>