<?php
require '../vendor/autoload.php';

use DebugBar\StandardDebugBar;

// $debugbar = new StandardDebugBar();
// $debugbarRenderer = $debugbar->getJavascriptRenderer();

if (isset($_POST['create_post'])) {

    $post_title = $_POST['title'];
    $post_author = $_POST['post_author'];
    $post_category_id = $_POST['post_category_id'];
    $post_status = $_POST['post_status'];

    $post_image = $_FILES['image']['name'];
    $post_image_temp = $_FILES['image']['tmp_name'];

    $post_tag = $_POST['post_tag'];
    $post_content = $_POST['post_content'];
    $post_date = date('d-m-y');
    // $post_comm_count = 0;

    // echo 'title '; echo $_POST['title'];echo "<br>";
    // echo 'post_author '; echo $_POST['post_author'];echo "<br>";
    // echo 'post_category_id '; echo $_POST['post_category_id'];echo "<br>";
    // echo 'post_status '; echo $_POST['post_status'];echo "<br>";

    // echo $_FILES['image']['name'];
    // echo $_FILES['image']['tmp_name'];

    // echo 'post_tag '; echo $_POST['post_tag'];echo "<br>";
    // echo 'post_content '; echo $_POST['post_content'];echo "<br>";
    // echo date('d-m-y');
    // exit;

    move_uploaded_file($post_image_temp, "../images/$post_image");

    $query = "INSERT INTO post(post_category_id, post_title, post_author, post_date, post_image, post_content, post_tag, post_status)";
    $query .= "VALUES({$post_category_id},'{$post_title}','{$post_author}',now(),'{$post_image}','{$post_content}','{$post_tag}','{$post_status}')";

    $create_post_query = mysqli_query($connection, $query);

    confirm($create_post_query);

    $the_post_id = mysqli_insert_id($connection);
    echo "<p class='bg-success' >Post Created <a href='../post.php?p_id={$the_post_id}'> View Post </a></p>";
}

?>



<form action="" method="post" enctype="multipart/form-data">

    <div class="form-group">
        <label for="title">Post Title</label>
        <input type="text" class="form-control" name="title">
    </div>

    <div class="form-group">
        <label for="category">Category</label>
        <select name="post_category_id" id="post_category">
            <?php
            $query = "SELECT * FROM categories";
            $select_categories = mysqli_query($connection, $query);

            confirm($select_categories);

            while ($row = mysqli_fetch_assoc($select_categories)) {
                $cat_id = $row['cat_id'];
                $cat_title = $row['cat_title'];

                echo "<option value='{$cat_id}'>{$cat_title}</option>";
            }
            ?>
        </select>
    </div>

    <!-- <div class="form-group">
        <label for="author">Post Author</label>
        <input type="text" class="form-control" name="author">
    </div> -->

    <div class="form-group">
        <label for="users">Users</label>
        <select name="post_author" id="post_category">
            <?php
            $query = "SELECT * FROM users";
            $select_users = mysqli_query($connection, $query);

            confirm($select_categories);

            while ($row = mysqli_fetch_assoc($select_users)) {
                $user_id = $row['user_id'];
                $username = $row['username'];

                echo "<option value='{$username}'>{$username}</option>";
            }
            ?>
        </select>
    </div>


    <div class="form-group">
        <select name="post_status" id="">
            <option value="draft">Draft</option>
            <option value="published">Publish</option>
        </select>
    </div>



    <div class="form-group">
        <label for="post_image">Post Image</label>
        <input type="file" name="image">
    </div>

    <div class="form-group">
        <label for="post_tag">Post Tags</label>
        <input type="text" class="form-control" name="post_tag">
    </div>

    <div class="form-group">
        <label for="post_content">Post Content</label>
        <textarea class="form-control" name="post_content" id="summernote" cols="30" rows="10"> </textarea>
    </div>

    <div class="form-group">
        <input class="btn btn-primary" type="submit" name="create_post" value="Publish Post">
    </div>


</form>