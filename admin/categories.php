<?php include "include/admin_header.php"; ?>

<div id="wrapper">

    <!-- Navigation -->
    <?php include "include/admin_navigation.php"; ?>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!--   Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Welcome to Admin Page
                        <small>categories</small>
                    </h1>

                    <div class="col-xs-6">
                        <?php insert_cat() ?>


                        <form action="" method="post">
                            <div class="form-group">
                                <label for="cat-title">Add Category</label>
                                <input type="text" class="form-control" name="cat_title">
                            </div>
                            <div>
                                <input class="btn btn-primary" type="submit" name="submit" value="Add Category">
                            </div>

                        </form>



                        <?php // UPDATE AND INCLUDE 

                        if (isset($_GET['edit'])) {
                            $cat_id = $_GET['edit'];
                            include "include/edit_categories.php";
                        }

                        ?>


                    </div>

                    <div class="col-xs-6">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Category title</th>
                                    <th>Id</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>

                                    <?php // find all categories query 
                                    findAllCategories()

                                    ?>

                                    <?php  // DELETE QUERY
                                    deleteAllCategories()


                                    ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

        <?php include "include/admin_footer.php"; ?>