<?php 
include "include/db.php";
include "include/header.php";
include_once "admin/functions.php";

$error = [];

$sucesso_ao_criar_user = false;

if (isset($_POST['submit'])) {

    // echo "working";

    $username = ($_POST['username']);
    $user_lastname = ($_POST['user_lastname']);
    $user_firstname = ($_POST['user_firstname']);
    $email = ($_POST['email']);
    $password = ($_POST['password']);


    if ($username == '') {

        $error['username'] = 'Usuário não pode ser vazio';
    }

    if ($user_firstname == '') {

        $error['user_firstname'] = 'Primeiro nome não pode ser vazio';
    }

    if ($user_lastname == '') {

        $error['user_lastname'] = 'Último nome não pode ser vazio';
    }

    if ($email == '') {

        $error['email'] = 'Email não pode ser vazio';
    }

    if ($password == '') {

        $error['password'] = 'Senha não pode ser vazio';
    }

    if (empty($error)) {

        if (!empty($username) && !empty($email) && !empty($password)) {

            $username = mysqli_real_escape_string($connection, $username);
            $user_firstname = mysqli_real_escape_string($connection, $user_firstname);
            $user_lastname = mysqli_real_escape_string($connection, $user_lastname);
            $email = mysqli_real_escape_string($connection, $email);
            $password = mysqli_real_escape_string($connection, $password);

            $query = "INSERT INTO users (username, user_firstname, user_lastname, user_email, user_password, user_role)";
            $query .= "VALUES('{$username}', '{$user_firstname}', '{$user_lastname}', '{$email}','{$password}','subscriber')";
            $register_user = mysqli_query($connection, $query);
            if (!"$register_user") {
                $error['campos_duplicados'] = mysqli_error($connection);
            } else {
                $sucesso_ao_criar_user = true;
            }
        }
    }
}
?>
<!-- Navigation -->

<?php include "include/navigation.php"; ?>


<!-- Page Content -->
<div class="container">

    <?php

        foreach ($error as $key => $value) {
            if (!empty($value)) {
                echo "<div>" . $value . "</div>";
            }
        }

        if ($sucesso_ao_criar_user) {
            echo "<div>Usuário criado com sucesso!</div>";
        }

    ?>

    <section id="login">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="form-wrap">
                        <h1>Cadastre Novo Usuário</h1>
                        <form role="form" action="registration.php" method="post" id="login-form" autocomplete="off">
                            <div class="form-group">
                                <!-- <h6 class="test-center"></h6><?php // echo "$message"
                                                                    ?></h6> -->
                                <label for="username" class="sr-only">Nome de usuário</label>
                                <input type="text" name="username" id="username" class="form-control" placeholder="Nome de Usuário" autocomplete="on" value="">
                            </div>
                            <div class="form-group">
                                <!-- <h6 class="test-center"></h6><?php // echo "$message"
                                                                    ?></h6> -->
                                <label for="user_firstname" class="sr-only">Primeiro Nome</label>
                                <input type="text" name="user_firstname" id="user_firstname" class="form-control" placeholder="Primeiro Nome" autocomplete="on" value="">
                            </div>
                            <div class="form-group">
                                <!-- <h6 class="test-center"></h6><?php // echo "$message"
                                                                    ?></h6> -->
                                <label for="user_lastname" class="sr-only">Último Nome</label>
                                <input type="text" name="user_lastname" id="user_lastname" class="form-control" placeholder="Primeiro Nome" autocomplete="on" value="">
                            </div>
                            <div class="form-group">
                                <label for="email" class="sr-only">Email</label>
                                <input type="email" name="email" id="email" class="form-control" placeholder="Email" autocomplete="on" value="">
                            </div>
                            <div class="form-group">
                                <label for="password" class="sr-only">Senha</label>
                                <input type="password" placeholder="Senha" name="password" id="key" class="form-control" >

                            </div>

                            <input type="submit" name="submit" id="btn-login" class="btn btn-primary btn-lg btn-block" value="Cadastrar">
                        </form>

                    </div>
                </div> <!-- /.col-xs-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>

    <?php include "include/footer.php"; ?>