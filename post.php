<?php
session_start();

include "include/db.php";
include "include/header.php"; ?>

<!-- Navigation -->
<?php include "include/navigation.php"; ?>


<?php

if(isset($_POST['liked'])){
    $post_id = $_POST['post_id'];
    // select post
    $searchpost = "SELECT * FROM post WHERE post_id=$post_id";
    $post_result = mysqli_query($connection,$query);
    $post = mysqli_fetch_array($post_result);
    $likes = $post['likes'];


    if(mysqli_num_rows($post_result) >= 1){
        echo $post['post_id'];
    }


}
?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">


            <?php
            if (isset($_GET['p_id'])) {

                $the_post_id = $_GET['p_id'];
                // $view_query = "UPDATE post SET post_view_counts = post_view_counts + 1 WHERE post_id = $the_post_id ";
                // $send_query = mysqli_query($connection, $view_query);
                // if (!$send_query) {
                //     die("FAILED" . mysqli_error($connection));
                // }



                $query = "SELECT * FROM post WHERE post_id = $the_post_id ";
                $select_all_post_query = mysqli_query($connection, $query);

                while ($row = mysqli_fetch_assoc($select_all_post_query)) {
                    $post_title = $row['post_title'];
                    $post_author = $row['post_author'];
                    $post_date = $row['post_date'];
                    $post_image = $row['post_image'];
                    $post_content = $row['post_content'];

            ?>
                    
                    <h2>
                        <a href="#"><?php echo $post_title; ?></a>
                    </h2>
                    <p class="lead">
                        by <a href="index.php"><?php echo $post_author; ?></a>
                    </p>
                    <p><span class="glyphicon glyphicon-time"></span> <?php echo "Postado em ", $post_date ?></p>
                    <hr>
                    <img class="img-responsive" src="images/<?php echo $post_image; ?>" alt="">
                    <hr>
                    <p><?php echo $post_content ?></p>
                    <hr>

                    <div class="row">
                        <p class="pull-right"><a href="#" class="like"><span class="glyphicon glyphicon-thumbs-up"></span>Like</a></p>
                    </div>
                    <div class="row">
                        <p class="pull-right">Curtidas: 10</p>
                    </div>

            <?php }
            } else {
                header("Location: index.php");
            }
            ?>


            <!-- Blog Comments -->

            <?php
            if (isset($_POST['create_comment'])) {
                $the_post_id = $_GET['p_id'];

                $comm_author = $_POST['comm_author'];
                $comm_email = $_POST['comm_email'];
                $comm_content = $_POST['comm_content'];


                if (!empty($comm_author) && !empty($comm_email) && !empty($comm_content)) {


                    $query = "INSERT INTO comments (comm_post_id, comm_author, comm_email, comm_content, comm_status, comm_date)";
                    $query .= "VALUES ($the_post_id, '{$comm_author}', '{$comm_email}', '{$comm_content}', 'disapproved', now())";

                    $create_comm_query = mysqli_query($connection, $query);
                    if (!$create_comm_query) {
                        die('FAILED' . mysqli_error($connection));
                    }
                    // $query = "UPDATE post SET post_comm_count = post_comm_count + 1 WHERE post_id = $the_post_id";
                    // $update_comm_count = mysqli_query($connection, $query);



                } else {
                    echo "<script>alert('fields cannot be empty ')</script>";
                }
            }

            ?>

            <!-- Comments Form -->
            <div class="well">
                <h4>Deixe um comentário:</h4>
                <form action="" method="post" role="form">

                    <div class="form-group">
                        <label for="Author">Nome</label>
                        <input type="text" class="form-control" name="comm_author">
                    </div>
                    <div class="form-group">
                        <label for="Email">Email</label>
                        <input type="email" class="form-control" name="comm_email">
                    </div>
                    <div class="form-group">
                        <label for="Comment">Comentário</label>
                        <textarea class="form-control" name="comm_content" rows="3"></textarea>
                    </div>
                    <button type="submit" name="create_comment" class="btn btn-primary">Enviar</button>
                </form>
            </div>

            <hr>

            <!-- Posted Comments -->
            <?php
            $query = "SELECT * FROM comments WHERE comm_post_id = {$the_post_id} AND comm_status = 'approved' ORDER BY comm_id DESC ";
            // $query .= "AND comm_status = 'approved' ";
            // $query .= "ORDER BY comm_id DESC ";q
            $select_comm_query = mysqli_query($connection, $query);
            if (!$select_comm_query) {
                die('FAILED' . mysqli_error($connection));
            }
            while ($row = mysqli_fetch_array($select_comm_query)) {
                $comm_date = $row['comm_date'];
                $comm_content = $row['comm_content'];
                $comm_author = $row['comm_author'];
            ?>



                <!-- Comment -->
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading"><?php echo $comm_author ?>
                            <small> <?php echo $comm_date ?></small>
                        </h4>
                        <?php echo $comm_content ?>
                    </div>
                </div>



            <?php } ?>




        </div>

        <!-- Blog Sidebar Widgets Column -->
        <?php include "include/sidebar.php"; ?>

    </div>
    <!-- /.row -->

    <hr>

    <!-- Footer -->
    <?php
    include "include/footer.php"; ?>


    <script>
        $(document).ready(function(){
            var post_id = <?php echo $the_post_id;?>
            var user_id = 1;
            $('.like').click(function(){
                $.ajax({
                    url: "post.php?p_id=<?php echo $the_post_id;?>",
                    type: 'post',
                    data: {
                        liked: 1,
                        'post_id': post_id,
                        'user_id': user_id

                    }
                    
                })


            })

        });
    </script>